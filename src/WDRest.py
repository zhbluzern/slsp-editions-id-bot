import requests
import json
import os
from dotenv import load_dotenv



class WDRest():
    def __init__(self):
        load_dotenv()
        self.headers = {"Accept": "application/json", "User-Agent": "User:Mfchris84-Bot (Adding SLSP-ID (P9907) through, chris.erlinger@scout.at)"}
        self.apiUrl = os.getenv("apiUrl")
        self.accessToken = os.getenv("accessToken")

    def getItem(self, qid):
        r = requests.get(url=f"{self.apiUrl}entities/items/{qid}")
        return r.json()
    
    def getStatement(self, qid, property):
        params = {"property":property} 
        r = requests.get(url=f"{self.apiUrl}entities/items/{qid}/statements", params=params, headers=self.headers)
        return r.json()

    def setStatement(self, property, value, qualifiers=[], references=[], rank="normal", comment="Add Statement"):
        statement = {"statement": { "rank" : rank, 
                                    "property": { "id": property },
                                    "value": { "content": value, "type": "value"},
                                    "qualifiers": qualifiers, 
                                    "references": references 
                                }, "tags": [], "bot": False, "comment": comment  }
        return statement
    
    def postStatement(self, qid, statement):
        headers = self.headers
        headers.update({"Authorization" : f"Bearer {self.accessToken}"})
        r = requests.post(url=f"{self.apiUrl}entities/items/{qid}/statements", json=statement, headers=headers)
        return r.json()

if __name__ == "__main__":
    api = WDRest()
    #print(api.getItem("Q67131535"))
    #print(json.dumps(api.getStatement("Q62666414", "P9907"), indent=2))

    #statement = api.setStatement("P9907", "991007047199705501", comment="Add SLSP editions ID by ISBN-Check") #Q106497091
    #qid="Q106497091"
    statement = api.setStatement("P98128", "991007047199705501", comment="Add SLSP editions ID by ISBN-Check") #Q233782 -> test.wikidata
    qid = "Q233782"
    print (json.dumps(statement, indent=2))
    print(api.postStatement(qid, statement, ))