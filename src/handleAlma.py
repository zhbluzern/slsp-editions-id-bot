import requests
from lxml import etree

#https://slsp-rzs.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK/
#?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=
#alma.isbn=978-3-319-11964-9+alma.main_pub_date=2014

class AlmaSRU():
    def __init__(self):
        self.sruUrl = "https://slsp-rzs.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK"
        self.params = { "version" : "1.2", "operation": "searchRetrieve","recordSchema" : "marcxml"}
        self.headers = {"Accept": "application/xml"}    
        self.ns = {'xmlns' : 'http://www.loc.gov/zing/srw/', 'slim' : 'http://www.loc.gov/MARC21/slim'} 

    def getSruResult(self, query):
        params = self.params
        params.update({"query":query})
        r = requests.get(url=self.sruUrl, params=params, headers=self.headers)
        self.root = etree.fromstring(r.content)
        return r
    
    def getNumOfRecs(self, result):
        nor = self.root.xpath(".//xmlns:numberOfRecords",namespaces=self.ns)
        return nor[0].text
    
    def getMetadataByXpath(self, xpath, rec=""):
        if rec == "":
            rec = self.root
        metadata = rec.xpath(xpath,namespaces=self.ns)
        return metadata
    
if __name__ == "__main__":
    alma = AlmaSRU()
    result = alma.getSruResult("alma.isbn=978-3-319-11964-9+alma.main_pub_date=2014")
    nor = alma.getNumOfRecs(result)
    slspId = alma.getMetadataByXpath(".//slim:controlfield[@tag='001']")
    print(slspId[0].text)
