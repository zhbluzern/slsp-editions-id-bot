# pip install sparqlwrapper
# https://rdflib.github.io/sparqlwrapper/

import sys
from SPARQLWrapper import SPARQLWrapper, JSON

endpoint_url = "https://query.wikidata.org/sparql"

query = """#title:Query ISBN and Publication Year for version/edition items (Q3331189) without SLSP-ID
SELECT ?item ?isbn (YEAR(?pubDate) AS ?pubYear) WHERE {
?item wdt:P31 wd:Q3331189.
  {
  OPTIONAL { ?item wdt:P212 ?isbn13. }
  OPTIONAL { ?item wdt:P957 ?isbn10.}
    BIND(COALESCE (?isbn13, ?isbn10) AS ?isbn) 
  }
  OPTIONAL { ?item wdt:P577 ?pubDate.}
  FILTER NOT EXISTS {?item wdt:P9907 ?slspId. }
  FILTER(STRSTARTS(?isbn, "978-3"))
}
LIMIT 100"""


def get_results(endpoint_url, query):
    user_agent = "Check for ISBN of version/edition items without SLSP-Editions-ID, User:Mfchris84 Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
    # TODO adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


if __name__ == "__main__":
    results = get_results(endpoint_url,query)
    for result in results["results"]["bindings"]:
        print(result)
