import src.WDSparql as sparql
import src.handleAlma as alma
import src.WDRest as wdrest
import re
alma = alma.AlmaSRU()

#1. Fetch all version/edition items in Wikidata without SLSP-ID (get isbn and pubyear)
itemsInWD = sparql.get_results(sparql.endpoint_url, sparql.query)

#2. Run through result set
for item in itemsInWD["results"]["bindings"]:
    #print (item)
    qid = re.sub("http://www.wikidata.org/entity/","",item['item']['value'])
    # 2.1 Build a SRU queryString with isbn and given publication year
    queryString = f"alma.isbn={item['isbn']['value']}"
    if "pubYear" in item.keys():
        queryString = f"{queryString}+alma.main_pub_date={item['pubYear']['value']}"

    # 2.2 Make the SRU-Request, if there is only one record found, we will add the SLSP-ID in Wikidata
    result = alma.getSruResult(queryString)
    nor = alma.getNumOfRecs(result)
    #print(f"For {queryString} found: {nor} records in SLSP-NZ")
    if int(nor) == 1:
        slspId = alma.getMetadataByXpath(".//slim:controlfield[@tag='001']")
        # 2.2.1 Add the SLSP-ID to the Wikidata-Item
        print(f"we found only one record for {qid} in SLSP, so we add it to Wikidata :: {slspId[0].text}")
        wdApi = wdrest.WDRest()
        statement = wdApi.setStatement("P9907", slspId[0].text, comment="Add SLSP editions ID by ISBN-Check") 
        print(wdApi.postStatement(qid, statement))
