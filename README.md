# SLSP Editions ID (P9907) Wikidata Bot

This python script allows a bot user on Wikidata to run as scheduled task for adding the Swisscovery Alma Networkzone Identifier to corresponding Wikidata Items. 

## Functionality 

1. The script makes a [SPARQL-Query][https://w.wiki/9JGy] for all version/edition items in Wikidata with an ISBN, optionally a publication date and without the SLSP ID.
2. With the ISBN and the publication year the script makes a request for records using the Swisscovery SRU API.
3. If there is exactly one record, the MMS-ID (MARC Field 001) will be parsed
4. The Script adds the MMS-ID in the corresponding Wikidata Item. (using the [Wikidata REST-API](https://www.wikidata.org/wiki/Wikidata:REST_API))